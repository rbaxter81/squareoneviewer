
macro(Square)

#**************************************** [SquareOne]

set(SQUARE_PATH ${CMAKE_SOURCE_DIR})

#**************************************** [Build]

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

set (SQUARE_PATH ${CMAKE_SOURCE_DIR})

#**************************************** [Deploy]

set (SQUARE_DEPLOY ${SQUARE_PATH}/deploy)

#**************************************** [DevOps]

set (SQUARE_DEVOPS ${SQUARE_PATH}/devops)

#**************************************** [Source]

set (SQUARE_SOURCE ${SQUARE_PATH}/source)

endmacro(Square)

#**************************************** [SquareOne]
# SquareOne
#**************************************** [Build]
# SquareOne/build
#**************************************** [Deploy]
# SquareOne/deploy
# SquareOne/deploy/square
# SquareOne/deploy/square/include
# SquareOne/deploy/square/image
# SquareOne/deploy/square/doc
# SquareOne/deploy/square/data
#**************************************** [DevOps]
# SquareOne/devops
# SquareOne/devops/cmake
# SquareOne/devops/script
#**************************************** [Source]
# SquareOne/source
# SquareOne/source/external
# SquareOne/source/products
# SquareOne/source/systems
# SquareOne/source/tools