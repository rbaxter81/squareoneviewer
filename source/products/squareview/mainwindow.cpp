#include "mainwindow.h"
#include "qlayout.h"
#include "qtoolbar.h"
#include "qtoolbutton.h"

/************************************************************ [CONSTRUCTOR LIST] <PUBLIC> */

MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent)
{
 // setWindowState(Qt::WindowMaximized);
  addToolBar(toolBar = new QToolBar());

  QToolButton *toolButton = new QToolButton();
  {
    toolButton->setText("File");
  }

  toolBar->addWidget(toolButton);

}