#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

class QScrollArea;
class QToolBar;
class QAction;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public: /****************************************************** [CONSTRUCTOR LIST] <PUBLIC> */

  MainWindow(QWidget *parent = 0);

  ~MainWindow(){};

  /************************************************************ [CLASS FUNCTIONS] <PUBLIC> */


private: /***************************************************** [CLASS VARIABLES] <PRIVATE> */

  QToolBar *toolBar;

};

#endif // end MAINWINDOW_H
